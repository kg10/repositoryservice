

## Requirements
- Docker

## Build on Your Local Machine
1. Clone the GitHub repository

    ```
	git clone https://kg10@bitbucket.org/kg10/repositoryservice.git
    ```
	
	
2. Run docker compose

	```
	docker-compose up -d --build
	```


3. Documentation page

	```
	http://localhost:8889/swagger-ui.html#
	```
	
	
		
 