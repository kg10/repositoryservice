package com.kg.githubapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class RepositoryApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepositoryApiApplication.class, args);
    }

}
