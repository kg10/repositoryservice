package com.kg.githubapi.service;

import com.kg.githubapi.dto.RepositoryUserDetailDto;

public interface RepositoryUserSearchService {

    RepositoryUserDetailDto findRepositoryUserDetailsByLogin(String login);
}
