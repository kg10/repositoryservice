package com.kg.githubapi.service;

import com.kg.githubapi.dto.GithubUserDto;
import com.kg.githubapi.dto.RepositoryUserDetailDto;
import com.kg.githubapi.event.model.RequestIdentificationEvent;
import com.kg.githubapi.mapper.RepositoryDtoMapper;
import com.kg.githubapi.restClient.GithubApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class RepositoryUserUserSearchServiceImpl implements RepositoryUserSearchService {

    private final GithubApi githubApi;
    private final RepositoryDtoMapper repositoryDtoMapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public RepositoryUserDetailDto findRepositoryUserDetailsByLogin(String login) {
        GithubUserDto githubUser = githubApi.findDetailUserByLogin(login);

        applicationEventPublisher.publishEvent(RequestIdentificationEvent.of(login));
        return repositoryDtoMapper.map(githubUser, makeCalculations(githubUser.getFollowers(), githubUser.getPublic_repos()));
    }

    private Float makeCalculations(Long amountFollowers, Long amountPublicRepositories) {
        if (Objects.isNull(amountFollowers) || Objects.isNull(amountPublicRepositories)) {
            log.warn("Invalid number followers or public repositories");
            return null;
        }
        if(amountFollowers == 0){
            log.warn("Division by zero is prohibited");
            return null;
        }

        return 6.0f / amountFollowers * (2 + amountPublicRepositories);
    }
}
