package com.kg.githubapi.repository;

import com.kg.githubapi.model.StatisticRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatisticRequestRepository extends JpaRepository<StatisticRequest, Long> {

    boolean existsByLogin(String login);

    Optional<StatisticRequest> findByLogin(String login);
}
