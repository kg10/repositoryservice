package com.kg.githubapi.event.model;

import lombok.Value;

@Value(staticConstructor = "of")
public class RequestIdentificationEvent {

    String login;

}
