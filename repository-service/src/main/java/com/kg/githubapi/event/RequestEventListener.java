package com.kg.githubapi.event;

import com.kg.githubapi.event.model.RequestIdentificationEvent;
import com.kg.githubapi.exception.type.NotFoundException;
import com.kg.githubapi.model.StatisticRequest;
import com.kg.githubapi.repository.StatisticRequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class RequestEventListener {

    private final StatisticRequestRepository statisticRequestRepository;

    @EventListener
    @Transactional
    public void handle(RequestIdentificationEvent event) {
        if (statisticRequestRepository.existsByLogin(event.getLogin())) {
            incrementRequestCountAndSaveByLogin(event.getLogin());
        } else {
            createFirstExecutionByLogin(event.getLogin());
        }
    }

    private void incrementRequestCountAndSaveByLogin(String login) {
        StatisticRequest statisticRequest = statisticRequestRepository.findByLogin(login)
                .orElseThrow(() -> new NotFoundException("Not found login"));
        statisticRequest.setRequestCount(statisticRequest.getRequestCount() + 1);
        statisticRequestRepository.save(statisticRequest);
    }


    private void createFirstExecutionByLogin(String login) {
        statisticRequestRepository.save(
                StatisticRequest.builder()
                        .login(login)
                        .requestCount(1L)
                        .build()
        );
    }
}
