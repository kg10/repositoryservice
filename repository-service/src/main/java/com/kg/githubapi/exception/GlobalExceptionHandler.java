package com.kg.githubapi.exception;

import com.kg.githubapi.exception.type.NotFoundException;
import com.kg.githubapi.exception.type.TimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public Error handleNotFoundException(NotFoundException e) {
        return Error.builder().message(e.getMessage()).build();
    }

    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ExceptionHandler(TimeoutException.class)
    public Error handleTimeoutException(TimeoutException e) {
        return Error.builder().message(e.getMessage()).build();
    }


}
