package com.kg.githubapi.exception.type;

import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PRIVATE;

@Getter
@FieldDefaults(level = PRIVATE)
public class TimeoutException extends RuntimeException {

    private String message;

    public TimeoutException(String message) {
        this.message = message;
    }
}
