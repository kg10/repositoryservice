package com.kg.githubapi.restClient;

import com.google.gson.Gson;
import com.kg.githubapi.dto.GithubUserDto;
import com.kg.githubapi.exception.type.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Component
@RequiredArgsConstructor
@Slf4j
public class GithubApi {

    private final HttpClient httpClient;
    private final Gson gson;

    @Value("${github.url}")
    private String githubUrl;

    public GithubUserDto findDetailUserByLogin(String login) {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(githubUrl + login))
                .build();

        try {
            String s = httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body).get(3L, TimeUnit.SECONDS);
            return gson.fromJson(s, GithubUserDto.class);

        } catch (TimeoutException e) {
            log.warn("Github repository timeout");
            throw new com.kg.githubapi.exception.type.TimeoutException("Timeout when call github repository");
        } catch (InterruptedException | ExecutionException e) {
            throw new NotFoundException("Exception when call github repository");
        }
    }
}
