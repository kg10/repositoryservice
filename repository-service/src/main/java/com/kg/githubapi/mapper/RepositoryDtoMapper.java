package com.kg.githubapi.mapper;

import com.kg.githubapi.dto.GithubUserDto;
import com.kg.githubapi.dto.RepositoryUserDetailDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface RepositoryDtoMapper {

    @Mappings({
            @Mapping(target = "avatarUrl", source = "githubUserDto.avatar_url"),
            @Mapping(target = "createdAt", source = "githubUserDto.created_at"),
            @Mapping(target = "calculations", source = "calculations")
    })
    RepositoryUserDetailDto map(GithubUserDto githubUserDto, Float calculations);
}
