package com.kg.githubapi.dto;

import io.swagger.annotations.ApiModel;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.ZonedDateTime;

import static lombok.AccessLevel.PRIVATE;

@ApiModel
@Data
@Builder
@Getter
@FieldDefaults(level = PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class GithubUserDto {

    Long id;
    String login;
    String name;
    String type;
    String avatar_url;
    ZonedDateTime created_at;
    Long followers;
    Long public_repos;
}
