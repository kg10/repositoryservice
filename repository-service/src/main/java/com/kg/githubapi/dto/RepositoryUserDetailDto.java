package com.kg.githubapi.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.time.ZonedDateTime;

import static lombok.AccessLevel.PRIVATE;

@ApiModel
@Data
@Getter
@Setter
@FieldDefaults(level = PRIVATE)
@NoArgsConstructor
public class RepositoryUserDetailDto {

    Long id;
    String login;
    String name;
    String type;
    String avatarUrl;
    ZonedDateTime createdAt;
    Float calculations;
}
