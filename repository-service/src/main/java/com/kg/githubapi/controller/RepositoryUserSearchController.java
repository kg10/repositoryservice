package com.kg.githubapi.controller;

import com.kg.githubapi.dto.RepositoryUserDetailDto;
import com.kg.githubapi.service.RepositoryUserSearchService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "Github repository info search", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class RepositoryUserSearchController {

    private final RepositoryUserSearchService repositoryUserSearchService;

    @GetMapping("/{login}")
    RepositoryUserDetailDto findRepositoryUserDetails(@PathVariable String login) {
        return repositoryUserSearchService.findRepositoryUserDetailsByLogin(login);
    }

}
