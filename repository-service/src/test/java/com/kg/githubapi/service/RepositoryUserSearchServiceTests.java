package com.kg.githubapi.service;

import com.kg.githubapi.dto.GithubUserDto;
import com.kg.githubapi.dto.RepositoryUserDetailDto;
import com.kg.githubapi.mapper.RepositoryDtoMapper;
import com.kg.githubapi.restClient.GithubApi;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest
public class RepositoryUserSearchServiceTests {

    @Autowired
    GithubApi githubApi;
    @Autowired
    RepositoryUserSearchService repositoryUserSearchService;

    @Test
    public void findDetailGithubRepositoryByMockedApi() {
        //given
        GithubUserDto response = buildSampleResponse();
        Mockito.when(
                githubApi.findDetailUserByLogin(anyString())
        ).thenReturn(response);

        // when
        GithubUserDto githubUser = githubApi.findDetailUserByLogin("");

        // then
        assertEquals(response.getAvatar_url(), githubUser.getAvatar_url());
        assertEquals(response.getCreated_at(), githubUser.getCreated_at());
        assertEquals(response.getId(), githubUser.getId());
        assertEquals(response.getLogin(), githubUser.getLogin());
        assertEquals(response.getName(), githubUser.getName());
        assertEquals(response.getType(), githubUser.getType());
    }

    @Test
    public void findRepositoryDetailsByNameAndOwner() {
        //given
        GithubUserDto response = buildSampleResponse();
        Mockito.when(
                githubApi.findDetailUserByLogin(anyString())
        ).thenReturn(response);

        // when
        RepositoryUserDetailDto dto = repositoryUserSearchService.findRepositoryUserDetailsByLogin("");

        // then
        assertEquals(response.getAvatar_url(), dto.getAvatarUrl());
        assertEquals(response.getCreated_at(), dto.getCreatedAt());
        assertEquals(response.getId(), dto.getId());
        assertEquals(response.getLogin(), dto.getLogin());
        assertEquals(response.getName(), dto.getName());
        assertEquals(response.getType(), dto.getType());
    }

    @Test
    public void findRepositoryDetailsByNameAndOwnerNotFound() {
        //given
        Mockito.when(
                githubApi.findDetailUserByLogin(anyString())
        ).thenReturn(GithubUserDto.builder().build());

        // when
        RepositoryUserDetailDto dto = repositoryUserSearchService.findRepositoryUserDetailsByLogin("");

        // then
        assertNull(dto.getAvatarUrl());
        assertNull(dto.getCreatedAt());
        assertNull(dto.getId());
        assertNull(dto.getLogin());
        assertNull(dto.getName());
        assertNull(dto.getType());
    }

    private GithubUserDto buildSampleResponse() {
        return GithubUserDto.builder()
                .avatar_url(RandomStringUtils.randomAlphabetic(8))
                .created_at(ZonedDateTime.now())
                .login(RandomStringUtils.randomAlphabetic(8))
                .name(RandomStringUtils.randomAlphabetic(8))
                .type(RandomStringUtils.randomAlphabetic(8))
                .id(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .followers(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .public_repos(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .build();
    }

    @Configuration
    public static class Config {

        @Bean
        GithubApi githubApi() {
            return Mockito.mock(GithubApi.class);
        }

        @Bean
        public RepositoryDtoMapper repositoryDtoMapper() {
            return Mappers.getMapper(RepositoryDtoMapper.class);
        }

        @Bean
        public RepositoryUserSearchService repositoryUserSearchService(GithubApi githubApi, RepositoryDtoMapper repositoryDtoMapper,
                                                                       ApplicationEventPublisher applicationEventPublisher) {
            return new RepositoryUserUserSearchServiceImpl(githubApi, repositoryDtoMapper, applicationEventPublisher);
        }
    }

}
