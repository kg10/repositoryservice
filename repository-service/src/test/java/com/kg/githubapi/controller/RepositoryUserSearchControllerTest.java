package com.kg.githubapi.controller;

import com.kg.githubapi.dto.GithubUserDto;
import com.kg.githubapi.restClient.GithubApi;
import com.kg.githubapi.service.RepositoryUserSearchService;
import com.kg.githubapi.service.RepositoryUserSearchServiceTests;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.time.ZonedDateTime;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@EnableWebMvc
@ContextConfiguration
public class RepositoryUserSearchControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    @Autowired
    private GithubApi githubApi;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext)
                .dispatchOptions(true).build();
    }

    @Test
    public void findDetailGithubRepository() throws Exception {
        //given
        GithubUserDto response = buildSampleResponse();
        Mockito.when(
                githubApi.findDetailUserByLogin(anyString())
        ).thenReturn(response);

        //when & then
        this.mockMvc.perform(get("/users/testUser"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.['avatarUrl']").value(response.getAvatar_url()))
                .andExpect(jsonPath("$.['createdAt']").isNotEmpty())
                .andExpect(jsonPath("$.['id']").value(response.getId()))
                .andExpect(jsonPath("$.['name']").value(response.getName()))
                .andExpect(jsonPath("$.['type']").value(response.getType()))
                .andExpect(jsonPath("$.['login']").value(response.getLogin()))
                .andReturn();
    }

    @Test
    public void findDetailGithubRepositoryEmpty() throws Exception {
        //given
        Mockito.when(
                githubApi.findDetailUserByLogin(anyString())
        ).thenReturn(GithubUserDto.builder().build());

        //when & then
        this.mockMvc.perform(get("/users/testUser"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.['avatarUrl']").isEmpty())
                .andExpect(jsonPath("$.['createdAt']").isEmpty())
                .andExpect(jsonPath("$.['id']").isEmpty())
                .andExpect(jsonPath("$.['name']").isEmpty())
                .andExpect(jsonPath("$.['type']").isEmpty())
                .andExpect(jsonPath("$.['login']").isEmpty())
                .andReturn();
    }

    private GithubUserDto buildSampleResponse() {
        return GithubUserDto.builder()
                .avatar_url(RandomStringUtils.randomAlphabetic(8))
                .created_at(ZonedDateTime.now())
                .login(RandomStringUtils.randomAlphabetic(8))
                .name(RandomStringUtils.randomAlphabetic(8))
                .type(RandomStringUtils.randomAlphabetic(8))
                .id(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .followers(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .public_repos(Long.parseLong(RandomStringUtils.randomNumeric(1)))
                .build();
    }

    @Configuration
    @Import({RepositoryUserSearchServiceTests.Config.class})
    public static class Config {

        @Bean
        RepositoryUserSearchController repositorySearchController(RepositoryUserSearchService repositoryUserSearchService) {
            return new RepositoryUserSearchController(repositoryUserSearchService);
        }
    }

}
